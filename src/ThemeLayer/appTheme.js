import { createMuiTheme } from "@material-ui/core/styles";

const palette = {
  primary: { main: "#2962ff" },
  secondary: { main: "#ffc107" },
  background: {
    default: "#ECEFF1"
  }
};

const typography = {
  htmlFontSize: 17
};

const themeName = "MDC Theme";

export default createMuiTheme({ palette, typography, themeName });
