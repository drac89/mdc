import React from "react";
import { shallow } from "enzyme";
import UtilityLayer from "./UtilityLayer";

it("renders without crashing", () => {
  shallow(
    <UtilityLayer>
      <div />
    </UtilityLayer>
  );
});
