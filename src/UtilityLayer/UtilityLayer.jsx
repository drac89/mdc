import React from "react";
import PropTypes from "prop-types";
import { SnackbarProvider } from "notistack";

const UtilityLayer = props => {
  const { children } = props;
  return (
    <SnackbarProvider maxSnack={3} preventDuplicate>
      {children}
    </SnackbarProvider>
  );
};

UtilityLayer.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default UtilityLayer;
