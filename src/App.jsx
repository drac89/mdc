import React from "react";
import UtilityLayer from "./UtilityLayer";
import ThemeLayer from "./ThemeLayer";
import ReduxLayer from "./ReduxLayer";
import ContactsPage from "./app/ContactsPage";

function App() {
  return (
    <ReduxLayer>
      <ThemeLayer>
        <UtilityLayer>
          <ContactsPage />
        </UtilityLayer>
      </ThemeLayer>
    </ReduxLayer>
  );
}

export default App;
