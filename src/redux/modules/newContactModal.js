const OPEN_MODAL = "newContactModal/OPEN_MODAL";
const CLOSE_MODAL = "newContactModal/CLOSE_MODAL";

const initialState = false;

export default (state = initialState, action) => {
  switch (action.type) {
    case OPEN_MODAL:
      return true;

    case CLOSE_MODAL:
      return false;

    default:
      return state;
  }
};

export const openModal = () => {
  return dispatch => {
    dispatch({
      type: OPEN_MODAL
    });
  };
};

export const closeModal = () => {
  return dispatch => {
    dispatch({
      type: CLOSE_MODAL
    });
  };
};
