import { combineReducers } from "redux";

// Import reducers
import contactDetailModal from "./contactDetailModal";
import newContactModal from "./newContactModal";
import contactList from "./contactList";
import filter from "./filter";

export default combineReducers({
  contactDetailModal,
  newContactModal,
  contactList,
  filter
});
