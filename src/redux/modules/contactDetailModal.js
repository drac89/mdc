const OPEN_MODAL = "contactDetailModal/OPEN_MODAL";
const CLOSE_MODAL = "contactDetailModal/CLOSE_MODAL";

const initialState = {
  open: false,
  contact: {
    id: 1,
    name: "",
    username: "",
    email: "",
    address: {
      street: "",
      suite: "",
      city: "",
      zipcode: "",
      geo: {
        lat: "",
        lng: ""
      }
    },
    phone: "",
    website: "",
    company: {
      name: "",
      catchPhrase: "",
      bs: ""
    }
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case OPEN_MODAL:
      return {
        ...state,
        open: true,
        contact: action.contact
      };

    case CLOSE_MODAL:
      return initialState;

    default:
      return state;
  }
};

export const openModal = contact => {
  return dispatch => {
    dispatch({
      type: OPEN_MODAL,
      contact
    });
  };
};

export const closeModal = () => {
  return dispatch => {
    dispatch({
      type: CLOSE_MODAL
    });
  };
};
