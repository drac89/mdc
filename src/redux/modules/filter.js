const SEARCH = "filter/SEARCH";

const initialState = {
  search: ""
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SEARCH:
      return { ...state, search: action.text };

    default:
      return state;
  }
};

export const search = text => {
  return dispatch => {
    dispatch({
      type: SEARCH,
      text
    });
  };
};
