const ADD_CONTACT = "contactList/ADD_CONTACT";
const DELETE_CONTACT = "contactList/DELETE_CONTACT";
const HAS_ERROR = "contactList/HAS_ERROR";
const IS_LOADING = "contactList/IS_LOADING";
const FETCH_SUCCESS = "contactList/FETCH_SUCCESS";

const initialState = {
  isLoading: false,
  hasError: false,
  contacts: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_CONTACT:
      return { ...state, contacts: [action.contact, ...state.contacts] };

    case DELETE_CONTACT: {
      // Remove deleted contact
      const newContacts = state.contacts.filter(
        contact => contact.id !== action.id
      );
      return {
        ...state,
        contacts: newContacts
      };
    }

    case HAS_ERROR:
      return { ...state, hasError: action.hasError };

    case IS_LOADING:
      return { ...state, isLoading: action.isLoading };

    case FETCH_SUCCESS:
      return { ...state, contacts: action.contacts };

    default:
      return state;
  }
};

export const addContact = contact => {
  return dispatch => {
    dispatch({
      type: ADD_CONTACT,
      contact
    });
  };
};

export const deleteContact = id => {
  return dispatch => {
    dispatch({
      type: DELETE_CONTACT,
      id
    });
  };
};

export function hasError(bool) {
  return {
    type: HAS_ERROR,
    hasError: bool
  };
}

export function isLoading(bool) {
  return {
    type: IS_LOADING,
    isLoading: bool
  };
}

export function fetchSuccess(contacts = []) {
  return {
    type: FETCH_SUCCESS,
    contacts
  };
}
