import axios from "axios";

const apiClient = axios.create({
  baseURL: "https://jsonplaceholder.typicode.com",
  timeout: 12000
});

export default {
  contacts: {
    list() {
      return apiClient.get("/users/");
    }
  }
};
