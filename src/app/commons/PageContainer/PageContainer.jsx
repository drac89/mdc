import React from "react";
import PropTypes from "prop-types";

function PageContainer({ children }) {
  return (
    <div style={{ padding: "4% 4% 8%", maxWidth: 600, margin: "auto" }}>
      {children}
    </div>
  );
}

PageContainer.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default PageContainer;
