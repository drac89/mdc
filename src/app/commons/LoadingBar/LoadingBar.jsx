import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import LinearProgress from "@material-ui/core/LinearProgress";

const styles = {
  root: {
    flexGrow: 1,
    position: "absolute",
    top: 0,
    zIndex: 99999,
    width: "100%"
  }
};

function LoadingBar({ classes }) {
  return (
    <div className={classes.root}>
      <LinearProgress color="primary" />
    </div>
  );
}

LoadingBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(LoadingBar);
