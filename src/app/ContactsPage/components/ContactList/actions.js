import axios from "../../../../axiosConfig";

export default async () => {
  try {
    const res = await axios.contacts.list();
    const { data } = res;
    return { data, error: false };
  } catch (err) {
    const {
      response: { status, statusText }
    } = err;
    return { error: { status, statusText } };
  }
};
