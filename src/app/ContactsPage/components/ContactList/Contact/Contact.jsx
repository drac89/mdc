/* eslint-disable react/jsx-wrap-multilines */
import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import IconButton from "@material-ui/core/IconButton";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import { deepOrange } from "@material-ui/core/colors";
import PhoneIcon from "@material-ui/icons/Phone";
import LocationIcon from "@material-ui/icons/LocationOn";
import DeleteIcon from "@material-ui/icons/DeleteForever";

const useStyles = makeStyles(theme => ({
  root: {
    borderBottom: `1px solid ${theme.palette.divider}`,

    "&:last-child": {
      borderBottom: "none"
    }
  },
  detail: {
    display: "flex",
    alignItems: "center"
  },
  avatar: {
    color: "#fff",
    background: deepOrange[500]
  },
  icon: {
    width: theme.typography.body1.fontSize,
    marginRight: 5
  }
}));

function Contact({ contact, onClick, onDelete }) {
  const classes = useStyles();

  return (
    <ListItem
      button
      onClick={() => onClick(contact)}
      key={contact.id}
      alignItems="flex-start"
      classes={{ container: classes.root }}
    >
      <ListItemAvatar>
        <Avatar className={classes.avatar} alt={contact.name} color="primary">
          {contact.name.slice(0, 1)}
        </Avatar>
      </ListItemAvatar>
      <ListItemText
        primary={contact.name}
        secondary={
          <>
            <Typography
              component="span"
              variant="body2"
              className={classes.detail}
              color="textSecondary"
            >
              <PhoneIcon className={classes.icon} />
              {contact.phone}
            </Typography>

            <Typography
              component="span"
              variant="body2"
              className={classes.detail}
              color="textSecondary"
            >
              <LocationIcon className={classes.icon} />
              {contact.address.city}
            </Typography>
          </>
        }
      />
      <ListItemSecondaryAction>
        {contact.new && (
          <IconButton
            edge="end"
            aria-label="Delete"
            onClick={() => onDelete(contact.id)}
          >
            <DeleteIcon />
          </IconButton>
        )}
      </ListItemSecondaryAction>
    </ListItem>
  );
}

Contact.propTypes = {
  contact: PropTypes.object.isRequired,
  onClick: PropTypes.func,
  onDelete: PropTypes.func
};

Contact.defaultProps = {
  onClick: () => {},
  onDelete: () => {}
};

export default Contact;
