/* eslint-disable react/jsx-wrap-multilines */
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import { withSnackbar } from "notistack";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListSubheader from "@material-ui/core/ListSubheader";
import Button from "@material-ui/core/Button";
import Contact from "./Contact";
import * as contactDetailModalActions from "../../../../redux/modules/contactDetailModal";
import * as contactListActions from "../../../../redux/modules/contactList";
import getContacts from "./actions";
import { LoadingBar } from "../../../commons";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    margin: "10px auto",
    backgroundColor: theme.palette.background.paper
  }
}));

const ContactList = props => {
  const { enqueueSnackbar, closeSnackbar } = props;
  const classes = useStyles();
  const contactList = useSelector(state => state.contactList);
  const search = useSelector(state => state.filter.search);
  const dispatch = useDispatch();

  // Fetch Contacts
  useEffect(() => {
    const fetchContactList = async () => {
      dispatch(contactListActions.isLoading(true));
      const response = await getContacts();
      dispatch(contactListActions.isLoading(false));
      if (response.data) {
        dispatch(contactListActions.fetchSuccess(response.data));
      } else {
        dispatch(contactListActions.hasError(true));
      }
    };
    fetchContactList();
  }, [dispatch]);

  const handleClick = contact => {
    dispatch(contactDetailModalActions.openModal(contact));
  };

  const handleDelete = id => {
    dispatch(contactListActions.deleteContact(id));
  };

  // Show a persistent snackbar in case of fetching error
  if (contactList.hasError) {
    const action = key => (
      <Button
        color="inherit"
        onClick={() => {
          closeSnackbar(key);
        }}
      >
        {"GOT IT"}
      </Button>
    );
    enqueueSnackbar("Oops.. Couldn't get contacts from the server", {
      variant: "error",
      persist: true,
      action
    });
  }

  // Apply search filter on contacts
  if (search) {
    let currentContacts = [];
    currentContacts = contactList.contacts;

    // Determine which items should be displayed
    // based on the search terms
    const filteredContacts = currentContacts.filter(contact => {
      // change contact name to lowercase
      const name = contact.name.toLowerCase();
      // change search term to lowercase
      const filter = search.toLowerCase();
      return name.includes(filter);
    });

    return (
      <List
        className={classes.root}
        subheader={<ListSubheader component="div">Search Result</ListSubheader>}
      >
        {filteredContacts.map(contact => (
          <Contact
            key={contact.id}
            contact={contact}
            onClick={handleClick}
            onDelete={handleDelete}
          />
        ))}
      </List>
    );
  }

  // Filter to get recently added contacts
  const recentContacts = contactList.contacts.filter(
    contact => contact.new === true
  );

  // Filter to get contacts coming from the api
  const allContacts = contactList.contacts.filter(
    contact => contact.new !== true
  );

  return (
    <>
      {/* Render new contacts list if there are new contacts */}
      {recentContacts.length > 0 && (
        <List
          className={classes.root}
          subheader={
            <ListSubheader component="div">New Contacts</ListSubheader>
          }
        >
          {recentContacts.map(contact => (
            <Contact
              key={contact.id}
              contact={contact}
              onClick={handleClick}
              onDelete={handleDelete}
            />
          ))}
        </List>
      )}

      {/* Render allContacts */}
      <List
        className={classes.root}
        subheader={
          <>
            {contactList.isLoading && <LoadingBar />}
            <ListSubheader component="div">All Contacts</ListSubheader>
          </>
        }
      >
        {allContacts.map(contact => (
          <Contact
            key={contact.id}
            contact={contact}
            onClick={handleClick}
            onDelete={handleDelete}
          />
        ))}
      </List>
    </>
  );
};

ContactList.propTypes = {
  enqueueSnackbar: PropTypes.func.isRequired,
  closeSnackbar: PropTypes.func.isRequired
};

export default withSnackbar(ContactList);
