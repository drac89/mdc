import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import AddIcon from "@material-ui/icons/AddCircle";
import * as filterActions from "../../../../redux/modules/filter";
import * as newContactModalActions from "../../../../redux/modules/newContactModal";

const useStyles = makeStyles({
  root: {
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
    width: "100%",
    marginBottom: 12
  },
  input: {
    marginLeft: 8,
    flex: 1
  },
  iconButton: {
    padding: 10
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4
  }
});

export default function Toolbar() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const searchValue = useSelector(state => state.filter.search);

  const handleSearch = event => {
    dispatch(filterActions.search(event.target.value));
  };

  return (
    <Paper className={classes.root} elevation={0} square>
      <InputBase
        className={classes.input}
        placeholder="Search for people"
        inputProps={{ "aria-label": "Search for people" }}
        value={searchValue}
        onChange={handleSearch}
      />
      <IconButton className={classes.iconButton} aria-label="Search">
        <SearchIcon />
      </IconButton>
      <Divider className={classes.divider} />
      <IconButton
        color="primary"
        className={classes.iconButton}
        aria-label="Add Contact"
        onClick={() => dispatch(newContactModalActions.openModal())}
      >
        <AddIcon />
      </IconButton>
    </Paper>
  );
}
