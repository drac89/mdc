import React from "react";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import { withSnackbar } from "notistack";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import * as contactDetailModalActions from "../../../../redux/modules/contactDetailModal";
import * as contactListActions from "../../../../redux/modules/contactList";

const useStyles = makeStyles(() => ({
  root: {
    width: "100%",
    maxWidth: 600,
    margin: "auto"
  }
}));

const ContactDetailModal = props => {
  const { enqueueSnackbar } = props;
  const classes = useStyles();
  const modal = useSelector(state => state.contactDetailModal);
  const { open: isOpen, contact } = modal;
  const dispatch = useDispatch();

  const handleClose = () => {
    dispatch(contactDetailModalActions.closeModal());
  };

  const handleDelete = () => {
    dispatch(contactListActions.deleteContact(contact.id));
    handleClose();
    enqueueSnackbar("Contact deleted");
  };

  const {
    address: { suite, street, city, zipcode }
  } = contact;

  return (
    <Dialog
      open={isOpen}
      classes={{ paper: classes.root }}
      onClose={handleClose}
      aria-labelledby="contact-detail-modal"
    >
      <DialogTitle id="contact-detail-modal">Contact Detail</DialogTitle>
      <DialogContent>
        <List>
          <ListItem>
            <ListItemText primary="Name" secondary={contact.name || "---"} />
          </ListItem>
          <ListItem>
            <ListItemText
              primary="Username"
              secondary={contact.username || "---"}
            />
          </ListItem>
          <ListItem>
            <ListItemText
              primary="Email Address"
              secondary={contact.email || "---"}
            />
          </ListItem>
          <ListItem>
            <ListItemText
              primary="Address"
              secondary={`${suite}, ${street}, ${city} ${zipcode}`}
            />
          </ListItem>
          <ListItem>
            <ListItemText primary="Phone" secondary={contact.phone || "---"} />
          </ListItem>
          <ListItem>
            <ListItemText
              primary="Website"
              secondary={contact.website || "---"}
            />
          </ListItem>
          <ListItem>
            <ListItemText primary="Company" secondary={contact.company.name} />
          </ListItem>
        </List>
      </DialogContent>
      <DialogActions>
        {contact.new && <Button onClick={handleDelete}>Delete Contact</Button>}
        <Button onClick={handleClose} color="primary">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
};

ContactDetailModal.propTypes = {
  enqueueSnackbar: PropTypes.func.isRequired
};

export default withSnackbar(ContactDetailModal);
