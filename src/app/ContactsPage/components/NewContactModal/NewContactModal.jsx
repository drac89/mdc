import React, { useState } from "react";
import PropTypes from "prop-types";
import { withSnackbar } from "notistack";
import { useSelector, useDispatch } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Typography } from "@material-ui/core";
import * as newContactModalActions from "../../../../redux/modules/newContactModal";
import * as contactListActions from "../../../../redux/modules/contactList";

const uuidv4 = require("uuid/v4");

const useStyles = makeStyles(() => ({
  root: {
    width: "100%",
    maxWidth: 600,
    margin: "auto"
  }
}));

const NewContactModal = props => {
  const { enqueueSnackbar } = props;
  const classes = useStyles();
  const isOpen = useSelector(state => state.newContactModal);
  const dispatch = useDispatch();
  const [values, setValues] = useState({});

  const handleChange = event => {
    event.persist();
    setValues(state => ({
      ...state,
      [event.target.id]: event.target.value
    }));
  };

  const handleClose = () => {
    dispatch(newContactModalActions.closeModal());
  };

  const handleSubmit = () => {
    const {
      username,
      name,
      email,
      phone,
      city,
      street,
      suite,
      zipcode,
      website,
      companyName
    } = values;
    const newContact = {
      username,
      name,
      email,
      phone,
      address: {
        city,
        street,
        suite,
        zipcode
      },
      website,
      company: {
        name: companyName
      },
      id: uuidv4(),
      new: true
    };

    dispatch(contactListActions.addContact(newContact));
    handleClose();
    enqueueSnackbar("Contact added");
  };

  return (
    <Dialog
      classes={{ paper: classes.root }}
      open={isOpen}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Create a new contact</DialogTitle>
      <DialogContent>
        <DialogContentText>
          To create a new contact please fill the form respectively.
        </DialogContentText>
        <form>
          <TextField
            autoFocus
            variant="outlined"
            margin="dense"
            id="username"
            label="Username"
            type="text"
            onChange={handleChange}
            fullWidth
            required
          />
          <TextField
            variant="outlined"
            margin="dense"
            id="name"
            label="Name"
            type="text"
            onChange={handleChange}
            fullWidth
            required
          />
          <TextField
            variant="outlined"
            margin="dense"
            id="email"
            label="Email Address"
            type="email"
            onChange={handleChange}
            fullWidth
            required
          />
          <TextField
            variant="outlined"
            margin="dense"
            id="phone"
            label="phone"
            type="text"
            onChange={handleChange}
            fullWidth
            required
          />
          <TextField
            variant="outlined"
            margin="dense"
            id="website"
            label="Website"
            type="text"
            onChange={handleChange}
            fullWidth
            required
          />

          <Typography>Address</Typography>
          <TextField
            variant="outlined"
            margin="dense"
            id="suite"
            label="Suite"
            type="text"
            onChange={handleChange}
            fullWidth
            required
          />
          <TextField
            variant="outlined"
            margin="dense"
            id="street"
            label="Street"
            type="text"
            onChange={handleChange}
            fullWidth
            required
          />
          <TextField
            variant="outlined"
            margin="dense"
            id="city"
            label="City"
            type="text"
            onChange={handleChange}
            fullWidth
            required
          />
          <TextField
            variant="outlined"
            margin="dense"
            id="zipcode"
            label="Zipcode"
            type="text"
            onChange={handleChange}
            fullWidth
            required
          />

          <Typography>Company</Typography>
          <TextField
            variant="outlined"
            margin="dense"
            id="companyName"
            label="Company Name"
            type="text"
            onChange={handleChange}
            fullWidth
            required
          />
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Cancel
        </Button>
        <Button onClick={handleSubmit} color="primary">
          Create
        </Button>
      </DialogActions>
    </Dialog>
  );
};

NewContactModal.propTypes = {
  enqueueSnackbar: PropTypes.func.isRequired
};

export default withSnackbar(NewContactModal);
