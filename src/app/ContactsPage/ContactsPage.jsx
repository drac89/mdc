import React from "react";
import {
  Toolbar,
  ContactList,
  NewContactModal,
  ContactDetailModal
} from "./components";
import PageContainer from "../commons/PageContainer";

export default function ContactsPage() {
  return (
    <PageContainer>
      <Toolbar />
      <ContactList />
      <NewContactModal />
      <ContactDetailModal />
    </PageContainer>
  );
}
