import React from "react";
import PropTypes from "prop-types";
import { Provider } from "react-redux";
import { throttle } from "lodash";
import store from "../redux/store";
import { saveState } from "../redux/localStorage";

store.subscribe(
  throttle(() => {
    saveState({
      // contacts: store.getState().contacts
    });
  }, 1000)
);

const ReduxLayer = props => {
  const { children } = props;
  return <Provider store={store}>{children}</Provider>;
};

ReduxLayer.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default ReduxLayer;
